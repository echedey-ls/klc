+++
title = "F7.6 THT hole diameter"
+++

* Hole sizes refer to the finished hole size after plating (as opposed to the drill size before plating)
* For soldered through-hole pads, minimum hole diameter is the maximum lead diameter plus `0.2mm`
  * This is derived from IPC-2222 class 2 recommendations
  * Maximum lead diameter is obtained from the device datasheet, and includes tolerances.

{{< klcimg src="hole_size_round_lead" title="Finished diameter must be at least lead size + 0.2mm" >}}